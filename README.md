# gitlab-api.sh

A simple macOS [httpie](https://httpie.io) wrapper for GitLab API.

## How to use it

```bash
./gitlab-api.sh METHOD PATH [params]
```

You could test it with `./gitlab-api.sh GET /user`, the first time it fail and tell you how to add your Personal Access Token to the macOS keychain.

### Configuration

It is possible to override the GitLab server url setting the `GITLAB_API_HOST` variable.

It is also possible to symlink the script with a name that matches the desired hostname, (i.e. `ls -s gitlab-api.sh gitlab.com`).
In this case it will default to HTTPS, if the symlink is `gdk.test` it will default to HTTP on port 3000.
