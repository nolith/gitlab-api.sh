#!/bin/bash

if ! hash http 2>/dev/null; then
    printf "You need httpie.\nCheck https://httpie.io for details or install with brew install httpie"
    exit 1
fi

app_name=$(basename "$0")
api_host="${GITLAB_API_HOST:-https://$app_name}"
case $api_host in
    "https://gdk.test")
        api_host="http://gdk.test:3000"
        ;;
    "https://gitlab-api.sh")
        api_host="https://gitlab.com"
        ;;
esac

pw_name="gitlab-api.sh"
if ! token=$(security find-generic-password -w -s "$pw_name" -a "$api_host"); then
    echo "could not get password, error $?"
    printf "You can add the password using:\nsecurity add-generic-password -s \"%s\" -a \"%s\" -w" "$pw_name" "$api_host"
    exit 1
fi

method=$1
path=$2
shift 2

exec http "$method" "$api_host/api/v4$path" "PRIVATE-TOKEN:$token" "$@"
